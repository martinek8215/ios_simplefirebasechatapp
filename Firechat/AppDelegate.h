//
//  AppDelegate.h
//  Firechat
//
//  Copyright (c) 2012 Firebase.

//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
