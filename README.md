# iOS Firebase Chat App

Firechat is a simple chat/messaging app that uses the Firebase iOS SDK.


# Install Guide

###First create your own Firebase app in Firebase.com.
###Replace FIREBASE_APP_URL in ViewController.m file according to own Firebase URL.(See Firebase Screen Shot)

For more information, please refer to Firebase [online doc](https://www.firebase.com/docs).